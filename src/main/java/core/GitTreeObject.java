package core;
import java.util.*;
import java.util.zip.*;
import java.io.*;

public class GitTreeObject extends AbstractGitObject{

    private HashMap<String,AbstractGitObject> contentMap;

    public GitTreeObject(String repoPath, String treeHash) throws IOException,WrongObjectTypeException{
        this.hash = treeHash;
        this.repoPath = repoPath;
        initContent();
    }

    private void initContent() throws IOException,WrongObjectTypeException{
        String objectPath;
        int i;
        InflaterInputStream inStream;

        this.contentMap = new HashMap();
        objectPath = repoPath + "/objects/" + this.hash.substring(0,2) + "/" + this.hash.substring(2);
        inStream = new InflaterInputStream(new FileInputStream(objectPath));

        this.type="";
        while((i = inStream.read()) != ' ') this.type+= (char) i;
        if (!this.type.equals("tree")) throw new WrongObjectTypeException();

        //Butta via il resto della prima riga:
        while((i = inStream.read()) != 0);

        //Legge le entry del tree
        while((i = inStream.read()) != -1){
            //Butta via i byte dei permessi
            while((i = inStream.read()) != ' ');
            String entryFilename = readEntryFilename(inStream);
            String entryHash = readEntryHash(inStream);
            String entryType = findOutEntryType(entryHash);
            if(entryType.equals("blob"))
                contentMap.put(entryFilename,new GitBlobObject(repoPath,entryHash));
            else if(entryType.equals("tree"))
                contentMap.put(entryFilename,new GitTreeObject(repoPath,entryHash));
        }
    }

    private String readEntryHash(InflaterInputStream inStream) throws IOException{
        int i;
        String entryHash="";
        for(int count = 0; count < 20 ; count++){
            i = inStream.read();
            entryHash += Integer.toHexString(i).length()<2?"0"+Integer.toHexString(i):Integer.toHexString(i);
        }
        return entryHash;
    }

    private String readEntryFilename(InflaterInputStream inStream) throws IOException{
        int i;
        String entryFilename="";
        while((i = inStream.read()) != 0){
            entryFilename += (char) i;
        }
        return entryFilename;
    }

    private String findOutEntryType(String hash) throws FileNotFoundException{
        String entryObjectPath = this.repoPath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2);
        Scanner scanner = new Scanner(new InflaterInputStream(new FileInputStream(entryObjectPath)));
        String entryType = scanner.next();
        scanner.close();
        return entryType;
    }

    public Set<String> getEntryPaths(){
        return contentMap.keySet();
    }

    public AbstractGitObject getEntry(String key){
        return contentMap.get(key);
    }

}
