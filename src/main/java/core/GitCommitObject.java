package core;
import java.util.*;
import java.util.zip.*;
import java.io.*;

public class GitCommitObject extends AbstractGitObject{

    private HashMap<String,String> contentMap;

    public GitCommitObject(String repoPath, String commitHash) throws FileNotFoundException,WrongObjectTypeException{
        this.repoPath = repoPath;
        this.hash = commitHash;
        initContent();
    }

    private void initContent() throws FileNotFoundException,WrongObjectTypeException{
        Scanner scanner;
        String line,objectPath;
        int index;

        this.contentMap = new HashMap();
        objectPath = repoPath + "/objects/" + this.hash.substring(0,2) + "/" + this.hash.substring(2);
        scanner = new Scanner(new InflaterInputStream(new FileInputStream(objectPath)));
        this.type = scanner.next();
        if (!this.type.equals("commit")) throw new WrongObjectTypeException();
        scanner.skip("[^\0]*\0");
        while(scanner.hasNext()){
            line = scanner.nextLine();
            index = line.indexOf(" ");
            if(index < 0)
                break;
            contentMap.put(line.substring(0,index),line.substring(index+1));
        }
    }

    public String getTreeHash(){
        return contentMap.get("tree");
    }

    public String getAuthor(){
        String author = contentMap.get("author");
        return author.substring(0,author.indexOf('>')+1);
    }

    public String getParentHash(){
        return contentMap.get("parent");
    }

}
