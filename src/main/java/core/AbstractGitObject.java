package core;
import java.util.*;
import java.util.zip.*;
import java.io.*;

public class AbstractGitObject{

    protected String repoPath;
    protected String hash;
    protected String type;

    public String getHash(){
        return hash;
    }

    public String getRepoPath(){
        return repoPath;
    }

    public String getType(){
        return type;
    }

}
