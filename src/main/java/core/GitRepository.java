package core;
import java.util.*;
import java.io.*;

public class GitRepository{

    private String repoPath;

    public GitRepository(String path){
        repoPath = path;
    }

    public String getHeadRef() throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(repoPath+"/HEAD"));
        scanner.skip("ref:");
        return scanner.next();
    }

    public String getRefHash(String ref) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(repoPath+"/"+ref));
        return scanner.next();
    }

}
