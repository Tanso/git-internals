package core;
import java.util.*;
import java.util.zip.*;
import java.io.*;

public class GitBlobObject extends AbstractGitObject{

    private String content="";

    public GitBlobObject(String repoPath,String hash) throws FileNotFoundException,WrongObjectTypeException{
        this.repoPath = repoPath;
        this.hash = hash;
        initContent();
    }

    private void initContent() throws FileNotFoundException,WrongObjectTypeException{
        String objectPath = repoPath + "/objects/" + this.hash.substring(0,2) + "/" + this.hash.substring(2);
        Scanner scanner = new Scanner(new InflaterInputStream(new FileInputStream(objectPath)));
        this.type = scanner.next();
        if (!this.type.equals("blob")) throw new WrongObjectTypeException();
        scanner.skip("[^\0]*\0");
        while(scanner.hasNext()){
            this.content += scanner.nextLine()+"\n";
        }
    }

    public String getContent(){
        return this.content;
    }

}
